# 1. Технические таблицы
create table admin1Codes
(
    code          text null,
    name          text null,
    name_2        text null,
    alternativeid int  null
);
create table alternateNames
(
    alternateNameId int  null,
    geonameid       int  null,
    isolanguage     text null,
    alternate_name  text null,
    isPreferredName int  null,
    isShortName     text null,
    isColloquial    text null,
    isHistoric      text null,
    `from`          text null,
    `to`            text null
);
create table timeZones
(
    CountryCode                      text   null,
    TimeZoneId                       text   null,
    `GMT offset 1. Jan 2023`         double null,
    `DST offset 1. Jul 2023`         double null,
    `rawOffset (independant of DST)` double null
);
create table locality
(
    id             bigint auto_increment primary key,
    parent_id      bigint       null,
    name           varchar(255) null,
    asciiname      varchar(255) null,
    alternatenames text         null,
    latitude       double       null,
    longitude      double       null,
    feature_class  char(1)      null,
    feature_code   varchar(10)  null,
    country_code   char(2)      null,
    cc2            varchar(200) null,
    admin1_code    varchar(20)  null,
    admin2_code    varchar(80)  null,
    admin3_code    varchar(20)  null,
    admin4_code    varchar(20)  null,
    population     bigint       null,
    elevation      integer      null,
    dem            integer      null,
    timezone       varchar(40)  null,
    updated_at     datetime     null,
    constraint id
        unique (id)
);
create table locality_city
(
    id             bigint auto_increment primary key,
    parent_id      bigint       null,
    name           varchar(255) null,
    asciiname      varchar(255) null,
    alternatenames text         null,
    latitude       double       null,
    longitude      double       null,
    feature_class  char(1)      null,
    feature_code   varchar(10)  null,
    country_code   char(2)      null,
    cc2            varchar(200) null,
    admin1_code    varchar(20)  null,
    admin2_code    varchar(80)  null,
    admin3_code    varchar(20)  null,
    admin4_code    varchar(20)  null,
    population     bigint       null,
    elevation      integer      null,
    dem            integer      null,
    timezone       varchar(40)  null,
    updated_at     datetime     null,
    constraint id
        unique (id)
);
create table locality_region
(
    id             bigint auto_increment primary key,
    parent_id      bigint       null,
    name           varchar(255) null,
    asciiname      varchar(255) null,
    alternatenames text         null,
    latitude       double       null,
    longitude      double       null,
    feature_class  char(1)      null,
    feature_code   varchar(10)  null,
    country_code   char(2)      null,
    cc2            varchar(200) null,
    admin1_code    varchar(20)  null,
    admin2_code    varchar(80)  null,
    admin3_code    varchar(20)  null,
    admin4_code    varchar(20)  null,
    population     bigint       null,
    elevation      integer      null,
    dem            integer      null,
    timezone       varchar(40)  null,
    updated_at     datetime     null,
    constraint id
        unique (id)
);

# 2. Локальные таблицы
create table city
(
    id            bigint auto_increment primary key,
    parent_id     bigint                             not null,
    name          varchar(255)                       not null,
    geo_position  varchar(255)                       not null,
    timezone      varchar(40)                        not null,
    utc_offset    int                                not null,
    region_code   varchar(20)                        not null,
    region_name   varchar(255)                       not null,
    country_code  char(2)                            not null,
    location_type varchar(10)                        not null,
    created_at    datetime default CURRENT_TIMESTAMP not null,
    updated_at    datetime default CURRENT_TIMESTAMP null,
    constraint id
        unique (id)
);
create table region
(
    id            bigint auto_increment primary key,
    parent_id     bigint                             not null,
    code          varchar(20)                        not null,
    name          varchar(255)                       not null,
    geo_position  varchar(255)                       not null,
    timezone      varchar(40)                        not null,
    utc_offset    int                                not null,
    country_code  char(2)                            not null,
    location_type varchar(10)                        not null,
    created_at    datetime default CURRENT_TIMESTAMP not null,
    updated_at    datetime default CURRENT_TIMESTAMP null,
    constraint id
        unique (id)
);
create table feature_codes
(
    code            text null,
    description     text null,
    all_description text null
);
create table modification
(
    id             bigint auto_increment primary key,
    parent_id      bigint       null,
    name           varchar(255) null,
    asciiname      varchar(255) null,
    alternatenames text         null,
    latitude       double       null,
    longitude      double       null,
    feature_class  char(1)      null,
    feature_code   varchar(10)  null,
    country_code   char(2)      null,
    cc2            varchar(200) null,
    admin1_code    varchar(20)  null,
    admin2_code    varchar(80)  null,
    admin3_code    varchar(20)  null,
    admin4_code    varchar(20)  null,
    population     bigint       null,
    elevation      integer      null,
    dem            integer      null,
    timezone       varchar(40)  null,
    updated_at     datetime     null,
    constraint id
        unique (id)
);

# 3. Заполнение таблиц

# Отделение городов от общего списка
INSERT INTO locality_city (id, parent_id, name, asciiname, alternatenames, latitude, longitude, feature_class,
                           feature_code, country_code, cc2, admin1_code, admin2_code, admin3_code, admin4_code,
                           elevation, population, dem, timezone, updated_at)
SELECT *
from locality l
where l.feature_class = 'P';

# Отделение административных делений от общего списка
INSERT INTO locality_region (id, parent_id, name, asciiname, alternatenames, latitude, longitude, feature_class,
                             feature_code, country_code, cc2, admin1_code, admin2_code, admin3_code, admin4_code,
                             elevation, population, dem, timezone, updated_at)
SELECT *
from locality l
where l.feature_class = 'A';

# Заполнение городов
INSERT INTO city (parent_id, name, geo_position, timezone, utc_offset, region_code, region_name, country_code,
                  location_type,
                  updated_at)
SELECT l.parent_id
     , alt_n.alternate_name
     , CONCAT_WS(';', l.latitude, l.longitude)
     , l.timezone
     , utc.`rawOffset (independant of DST)`
     , adm.code
     , alt_n_region.alternate_name
     , l.country_code
     , CONCAT_WS('.', l.feature_class, l.feature_code)
     , l.updated_at
FROM locality_city l
         JOIN alternateNames alt_n ON l.parent_id = alt_n.geonameid
         JOIN admin1Codes adm on adm.code = CONCAT_WS('.', l.country_code, l.admin1_code)
         JOIN alternateNames alt_n_region ON adm.alternativeid = alt_n_region.geonameid
         JOIN timeZones utc ON utc.TimeZoneId = l.timezone
WHERE alt_n.isolanguage = 'ru'
  AND alt_n_region.isolanguage = 'ru';

# Заполнение региона
INSERT INTO region (parent_id, code, name, geo_position, timezone, utc_offset, country_code, location_type, updated_at)
SELECT l.parent_id
     , adm.code
     , alt_n.alternate_name
     , CONCAT_WS(';', l.latitude, l.longitude)
     , l.timezone
     , utc.`rawOffset (independant of DST)`
     , l.country_code
     , CONCAT_WS('.', l.feature_class, l.feature_code)
     , l.updated_at
FROM locality_region l
         JOIN alternateNames alt_n ON l.parent_id = alt_n.geonameid
         JOIN admin1Codes adm on CONCAT_WS('.', l.country_code, l.admin1_code) = adm.code
         JOIN timeZones utc ON utc.TimeZoneId = l.timezone
WHERE 1 = 1
  AND alt_n.isolanguage = 'ru'
  AND l.feature_code = 'ADM1';

# 4. Удаление технических таблиц
DROP TABLE locality;
DROP TABLE admin1Codes;
DROP TABLE alternateNames;
DROP TABLE timeZones;
DROP TABLE locality_city;
DROP TABLE locality_region;


# 5. Борьба с дубликатами

# Удаление наименований с англ. буквами
DELETE
FROM city
WHERE name REGEXP '[A-z]';

# Просмотр дубликатов по parent_id
WITH DuplicateValue AS (SELECT parent_id, COUNT(*)
                        FROM city s
                        GROUP BY parent_id
                        HAVING COUNT(*) > 1)
SELECT *
FROM city
WHERE parent_id IN (SELECT parent_id FROM DuplicateValue)
ORDER BY parent_id;


create table city_new
(
    id            int auto_increment primary key,
    parent_id     bigint                             not null,
    name          varchar(255)                       not null,
    geo_position  varchar(255)                       not null,
    timezone      varchar(40)                        not null,
    utc_offset    int                                not null,
    region_code   varchar(20)                        not null,
    region_name   varchar(255)                       not null,
    country_code  char(2)                            not null,
    location_type varchar(10)                        not null,
    updated_at    datetime default CURRENT_TIMESTAMP null,
    unique (parent_id)
);

INSERT IGNORE INTO city_new (parent_id, name, geo_position, timezone, utc_offset, region_code, region_name,
                             country_code,
                             location_type, updated_at)
SELECT DISTINCT parent_id,
                name,
                geo_position,
                timezone,
                utc_offset,
                region_code,
                region_name,
                country_code,
                location_type,
                updated_at
FROM city;

DROP TABLE city;

RENAME TABLE city_new to city;