# Справочник городов РФ
Информация о геопозиции, часовых поясах, регионах и городах РФ.

За основу взят ресурс: http://www.geonames.org/

Данные на ресурсе обновляются каждый день, можно сделать механики на уровне кода, которые будут подтягивать данные с ресурса и обновлять в локальных таблицах

---
### Содержание репозитории
`ready_data` - репозитория с готовыми данными для загрузки в БД MySql в формате CSV актуальными на <span style="color: red">21.05.2023 !</span>
- 183 231 населенных пунктов
- 202 региона

`script.sql` - скрипт для формирования данных из указанного ресурса по шагам 

`README.md` - этот файл

---
### Инструкция по формированию БД по скрипту
1. Запустить раздел `1. Технические таблицы` из файла `script.sql`
2. Запустить раздел `2. Локальные таблицы` из файла `script.sql`
3. Импортировать данные с ресурса в таблицы:
   - Таблица `admin1Codes` <-- Данные http://download.geonames.org/export/dump/admin1CodesASCII.txt
   - Таблица `alternateNames` <-- Данные http://download.geonames.org/export/dump/alternatenames/ выбрать папку с нужным языком
   - Таблица `timeZones` <-- Данные http://download.geonames.org/export/dump/timeZones.txt
   - Таблица `locality` <-- Данные http://download.geonames.org/export/dump/ выбрать папку с нужным языком
4.  Запустить раздел `3. Заполнение таблиц` из файла `script.sql`
5.  Запустить раздел `4. Удаление технических таблиц` из файла `script.sql`

---
### В результате получаются след. таблицы с данными
`city`

| id | parent_id |   name    |  geo_position   |      timezone      | utc_offset | region_code | region_name         | country_code | location_type |     created_at      |     updated_at      |
|:--:|:---------:|:---------:|:---------------:|:------------------:|:----------:|:-----------:|---------------------|:------------:|:-------------:|:-------------------:|:-------------------:|
| 1  |  461753   | Зюраткуль | 54.9259;59.2314 | Asia/Yekaterinburg |     5      |    RU.13    | Челябинская область |      RU      |     P.PPL     | 2023-05-21 00:51:24 | 2017-02-11 00:00:00 |


`region`

| id | parent_id | code  |        name         | geo_position |   timezone    | utc_offset | country_code | location_type |     created_at      |     updated_at      |
|:--:|:---------:|:-----:|:-------------------:|:------------:|:-------------:|:----------:|:------------:|:-------------:|:-------------------:|:-------------------:|
| 1  |  468898   | RU.88 | Ярославская область |   58;39.5    | Europe/Moscow |     3      |      RU      |    A.ADM1     | 2023-05-21 00:51:31 | 2023-01-12 00:00:00 |


`feature_codes`

|  code  |                    description                    |                                   all_description                                   |
|:------:|:-------------------------------------------------:|:-----------------------------------------------------------------------------------:|
| A.ADM1 | политико-административное деление первого порядка | "первоначальное политико-административное  деление, например, деление США на штаты" |
